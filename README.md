**This is a serial codes of Brownian dynamics simulation of polymer chain with hydrodynamic interactions and calculate static structure factor,  written in C.**

Ref: Bo Liu, J. Chem. Phys. 118, 8061
(2003).
   
 	 

 *	 Compile code using: 
 *	 	 ./scriptname.sh

      



 *	 Run :
 *		 nohup ./chain < chain.in &


 ![Struct](/uploads/12a6379761035458b63c2aa5e7f29a7c/Struct.png)			
 
The static structure factor of the chain, for N=6 (lowest curve), 8, 11, 15, 20, 25,
30, 35, 40, 45, 50 (highest curve).
     
 Written By::<br/>
       **Rajneesh Kumar**
 *	Theoretical Sciences Unit, Jawaharlal Nehru Centre for Advanced Scientific Research,
 *	Bengaluru 560064, India.
 *	Email: rajneesh[at]jncasr.ac.in
 *	27 Dec, 2020

